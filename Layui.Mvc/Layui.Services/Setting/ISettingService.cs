﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Layui.Services.Setting
{
    public interface ISettingService
    {
        /// <summary>
        /// 获取所有并缓存
        /// </summary>
        /// <returns></returns>
        List<Entities.Setting> GetAllCache();

        /// <summary>
        /// 保存基数设置的值
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        (bool Status,string Message) SaveSetting(string name, string value);
    }
}
