﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Layui.Entities;

namespace Layui.Services.SysPermission
{
    public class SysPermissionService : ISysPermissionService
    {
        private IMemoryCache _memoryCache;

        private const string MODEL_KEY = "Layui.services.sysPermission";
        private LayuiDbContext _LayuiDbContext;


        public SysPermissionService(LayuiDbContext LayuiDbContext,
            IMemoryCache memoryCache)
        {
            this._LayuiDbContext = LayuiDbContext;
            this._memoryCache = memoryCache;
        }

        /// <summary>
        /// 获取所有数据并缓存
        /// </summary>
        /// <returns></returns>
        public List<Entities.SysPermission> GetAllCache()
        {
            List<Entities.SysPermission> list = null;
            _memoryCache.TryGetValue<List<Entities.SysPermission>>(MODEL_KEY, out list);
            if (list != null) return list;
            list = _LayuiDbContext.SysPermissions.ToList();
            _memoryCache.Set(MODEL_KEY, list, DateTimeOffset.Now.AddDays(1));
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Entities.SysPermission> GetByRoleId(Guid id)
        {
            var list = GetAllCache();
            if (list == null) return null;
            return list.Where(o => o.RoleId == id).ToList();
        }

        /// <summary>
        /// 保存角色权限数据
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="categoryIds"></param>
        /// <param name="creator"></param>
        public void SaveRolePermission(Guid roleId, List<int> categoryIds, Guid creator)
        {
            var list = _LayuiDbContext.SysPermissions.Where(o => o.RoleId == roleId);
            if (categoryIds == null || !categoryIds.Any())
                _LayuiDbContext.SysPermissions.RemoveRange(list);
            else
            {
                foreach (int categoryId in categoryIds)
                {
                    var item = list.FirstOrDefault(o => o.CategoryId == categoryId);
                    if (item == null)
                    {
                        _LayuiDbContext.SysPermissions.Add(new Entities.SysPermission()
                        {
                            Id = Guid.NewGuid(),
                            RoleId = roleId,
                            CreationTime = DateTime.Now,
                            Creator = creator,
                            CategoryId = categoryId
                        });
                    }
                }
                foreach (var del in list)
                    if (!categoryIds.Any(o => o == del.CategoryId))
                        _LayuiDbContext.SysPermissions.Remove(del);
            }
            _LayuiDbContext.SaveChanges();
            RemoveCache();
        }

        /// <summary>
        /// 移除缓存
        /// </summary>
        public void RemoveCache()
        {
            _memoryCache.Remove(MODEL_KEY);
        }
    }
}
