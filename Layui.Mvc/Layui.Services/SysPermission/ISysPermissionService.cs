﻿using System;
using System.Collections.Generic;
using System.Text;
using Layui.Entities;

namespace Layui.Services.SysPermission
{
    public interface ISysPermissionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<Entities.SysPermission> GetAllCache();
         
        /// <summary>
        /// 移除缓存
        /// </summary>
        void RemoveCache();

        /// <summary>
        /// 获取角色权限
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<Entities.SysPermission> GetByRoleId(Guid id);

        /// <summary>
        /// 保存角色权限数据
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="categoryIds"></param>
        /// <param name="creator"></param>
        void SaveRolePermission(Guid roleId, List<int> categoryIds, Guid creator);
    }
}
