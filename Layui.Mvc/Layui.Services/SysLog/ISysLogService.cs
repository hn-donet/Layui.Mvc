﻿using Layui.Core;
using Layui.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Layui.Services.SysLog
{
    public interface ISysLogService
    {
        /// <summary>
        /// 清除日志
        /// </summary>
        void ClearLog();

        /// <summary>
        /// 插入系统日志
        /// </summary>
        /// <param name="level"></param>
        /// <param name="shortMessage"></param>
        /// <param name="fullMessage"></param>
        /// <returns></returns>
        Entities.SysLog InsertLog(EnumLevel level, string shortMessage, string fullMessage = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        void Debug(string message, Exception exception = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        void Information(string message, Exception exception = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        void Warning(string message, Exception exception = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        void Error(string message, Exception exception = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        void Fatal(string message, Exception exception = null);

        /// <summary>
        /// 查询日志
        /// </summary>
        /// <param name="arg"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        IPagedList<Entities.SysLog> Search(LogSearchArg arg, int page, int size);
    }
}
