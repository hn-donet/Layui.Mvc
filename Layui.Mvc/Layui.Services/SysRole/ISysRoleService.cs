﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Layui.Services.SysRole
{
    public interface ISysRoleService
    {
        /// <summary>
        /// 获取所有的roles数据
        /// 并缓存
        /// </summary>
        /// <returns></returns>
        List<Entities.SysRole> GetAllRoles();
         
        /// <summary>
        /// 获取角色详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Entities.SysRole GetRole(Guid id);

        /// <summary>
        /// 保存新增角色
        /// </summary>
        /// <param name="role"></param>
        void InserRole(Entities.SysRole role);

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="roleId"></param>
        void DeleteRole(Guid roleId);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="role"></param>
        void UpdateRole(Entities.SysRole role);

    }
}
