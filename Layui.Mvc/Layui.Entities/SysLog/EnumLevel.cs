﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Layui.Entities
{
    public enum EnumLevel
    {
        /// <summary>
        /// 调试
        /// </summary>
        Debug = 10,

        /// <summary>
        /// 一般
        /// </summary>
        Information = 20,

        /// <summary>
        /// 警告
        /// </summary>
        Warning = 30,

        /// <summary>
        /// 错误
        /// </summary>
        Error = 40,

        /// <summary>
        /// 致命
        /// </summary>
        Fatal = 50
    }
}
