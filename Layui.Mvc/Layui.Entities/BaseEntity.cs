﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Layui.Entities
{
    public class BaseEntity
    {
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }
    }
}
