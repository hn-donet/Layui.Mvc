﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Layui.Entities
{
    public class LayuiDbContext: DbContext
    {
        public LayuiDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<SysUser> SysUsers { get; set; }
        public DbSet<SysUserToken> SysUserTokenes { get; set; }
        public DbSet<SysUserLoginLog> SysUserLoginLogs { get; set; }
        public DbSet<SysUserRole> SysUserRoles { get; set; }
        public DbSet<SysPermission> SysPermissions { get; set; }
        public DbSet<SysRole> SysRoles { get; set; }
        public DbSet<SysLog> SysLogs { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<SysUserNote> SysUserNotes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddConfiguration(new SysUserMap());
        }
    }
}
