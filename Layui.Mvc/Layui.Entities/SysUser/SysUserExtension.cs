﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Layui.Entities
{
    public partial class SysUser
    {

        /// <summary>
        /// 角色列表
        /// </summary>
        [NotMapped]
        public List<SysRole> SysRoles { get; set; }


    }
}
