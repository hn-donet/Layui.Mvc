﻿using Layui.Core;
using Layui.Framework.Controllers.Admin;
using Layui.Framework.Infrastructure;
using Layui.Framework.Security.Admin;
using Layui.Mvc.Areas.Admin.Controllers.Models;
using Layui.Services.SysUser;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Layui.Mvc.Areas.Admin.Controllers
{
    /// <summary>
    /// 个人中心
    /// </summary>
    [Route("admin/profile")]
    public class ProfileController : PublicAdminController
    {
        // GET: Profile

        private ISysUserService _sysUserService;
        private IWorkContext _workContext;
        private IAdminAuthService _adminAuthService;

        public ProfileController(ISysUserService sysUserService,
            IWorkContext workContext,
            IAdminAuthService adminAuthService)
        {
            this._adminAuthService = adminAuthService;
            this._workContext = workContext;
            this._sysUserService = sysUserService;
        }

        /// <summary>
        /// 个人首页
        /// </summary>
        /// <returns></returns>
        [Route("", Name = "profileIndex")]
        public ActionResult ProfileIndex()
        {
            return View(_workContext.CurrentUser);
        }

        /// <summary>
        /// 上传头像
        /// </summary>
        /// <param name="avatar"></param>
        /// <returns></returns>
        [Route("avatarUpload", Name = "avatarUpload")]
        [HttpPost]
        public JsonResult AvatarUpload(IFormFile avatar)
        {
            if (avatar == null)
                return Json(new { message = "请选择头像图片" });
            using (var stream = avatar.OpenReadStream())
            {
                byte[] bt = new byte[avatar.Length];
                stream.Read(bt, 0, bt.Length);
                _sysUserService.AddAvatar(_workContext.CurrentUser.Id, bt);
                return Json(new { status = "OK", message = "上传成功", url = Url.RouteUrl("avatar") });
            }
        }

        /// <summary>
        /// 个人头像预览连接
        /// </summary>
        /// <returns></returns>
        [Route("avatar", Name = "avatar")]
        public void Avatar()
        {
            var bt = _workContext.CurrentUser.Avatar;
            if (bt == null)
                return;
            Response.ContentType = "image/jpeg";
            Response.Body.Write(bt, 0, bt.Length);
        }

        /// <summary>
        /// 修改个人资料
        /// </summary>
        /// <returns></returns>
        [Route("changeInfo", Name = "changeInfo")]
        [HttpPost]
        public JsonResult ChangeInfo(Entities.SysUser model)
        {
            if (!ModelState.IsValid)
            {
                AjaxData.Message = "数据验证未通过";
                return Json(AjaxData);
            }
            model.Name = model.Name.Trim();
            model.Id = _workContext.CurrentUser.Id;
            model.Modifier = _workContext.CurrentUser.Id;
            _sysUserService.UpdateSysUser(model);
            AjaxData.Message = "个人资料修改保存成功";
            AjaxData.Status = true;
            return Json(AjaxData);
        }

        /// <summary>
        /// 个人修改密码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("changePwd", Name = "changPassword")]
        [HttpPost]
        public JsonResult ChangePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                AjaxData.Message = "数据验证未通过";
                return Json(AjaxData);
            }
            model.Password = model.Password.Trim();
            _sysUserService.ChangePassword(_workContext.CurrentUser.Id, model.Password);
            AjaxData.Status = true;
            AjaxData.Message = "密码修改成功，下次登录请使用新密码。";
            return Json(AjaxData);
        }

        /// <summary>
        /// 退出系统
        /// </summary>
        /// <returns></returns>
        [Route("out", Name = "adminSignOut")]
        public ActionResult SignOut()
        {
            _adminAuthService.SignOut();
            return RedirectToRoute("adminLogin");
        }

    }
}