﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Layui.Framework.Controllers.Admin;
using Layui.Services.SysRole;
using Layui.Framework.Menu;
using Layui.Services.SysPermission;
using Layui.Entities;
using Layui.Services.Category;
using Layui.Framework.Infrastructure;

namespace Layui.Mvc.Areas.Admin.Controllers
{
    [Route("admin/role")]
    public class RoleController : AdminPermissionController
    {

        private ISysRoleService _sysRoleService;
        private ISysPermissionService _sysPermissionService;
        private ICategoryService _categoryService;
        private IWorkContext _workContext;

        public RoleController(ISysRoleService sysRoleService,
            ICategoryService categoryService,
            IWorkContext workContext,
            ISysPermissionService sysPermissionService)
        {
            this._workContext = workContext;
            this._categoryService = categoryService;
            this._sysRoleService = sysRoleService;
            this._sysPermissionService = sysPermissionService;
        }


        /// <summary>
        /// 角色列表
        /// </summary>
        /// <returns></returns>
        [Route("", Name = "roleIndex")]
        public IActionResult RoleIndex()
        {
            return View(_sysRoleService.GetAllRoles());
        }

        /// <summary>
        /// 编辑角色
        /// </summary>
        /// <returns></returns>
        [Route("edit", Name = "editRole")]
        public IActionResult EditRole(Guid? id = null)
        {
            Entities.SysRole model = null;
            if (id.HasValue && id.Value != Guid.Empty)
                model = _sysRoleService.GetRole(id.Value);
            return PartialView(model);
        }
        [Route("edit")]
        [HttpPost]
        public ActionResult EditRole(Entities.SysRole model)
        {
            ModelState.Remove("Id");
            if (!ModelState.IsValid)
                return View(model);
            model.Name = model.Name.Trim();
            if (model.Id == Guid.Empty)
            {
                model.Id = Guid.NewGuid();
                model.CreationTime = DateTime.Now;
                model.Creator = _workContext.CurrentUser.Id;
                _sysRoleService.InserRole(model);
            }
            else
            {
                model.ModifiedTime = DateTime.Now;
                model.Modifier = _workContext.CurrentUser.Id;
                _sysRoleService.UpdateRole(model);
            }
            AjaxData.Status = true;
            AjaxData.Message = "保存成功";
            return Json(AjaxData);
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("delete/{id}", Name = "deleteRole")]
        public JsonResult DeleteRole(Guid id)
        {
            _sysRoleService.DeleteRole(id);
            AjaxData.Status = true;
            AjaxData.Message = "角色已删除成功";
            return Json(AjaxData);
        }

        /// <summary>
        /// 角色权限设置
        /// </summary>
        /// <returns></returns>
        [Route("permission", Name = "rolePermission")]
        [HttpGet]
        public ActionResult RolePermission(Guid id)
        {
            RolePermissionViewModel model = new RolePermissionViewModel();
            model.CategoryList = _categoryService.GetAllCache();
            var roleList = _sysRoleService.GetAllRoles();
            if (roleList != null && roleList.Any())
            {
                model.Role = roleList.FirstOrDefault(o => o.Id == id);
                model.RoleList = roleList;
                model.Permissions = _sysPermissionService.GetByRoleId(id);
            }
            return View(model);
        }

        [HttpPost]
        [Route("permission")]
        public JsonResult RolePermission(Guid id, List<int> sysResource)
        {
            _sysPermissionService.SaveRolePermission(id, sysResource, _workContext.CurrentUser.Id);
            AjaxData.Status = true;
            AjaxData.Message = "角色权限设置成功";
            return Json(AjaxData);
        }
    }
}