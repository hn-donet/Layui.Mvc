﻿using Layui.Framework.Controllers.Admin;
using Layui.Services.Setting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Layui.Mvc.Areas.Admin.Controllers
{
    [Route("admin/setting")]
    public class SettingController : AdminPermissionController
    {
        // GET: Setting
        private ISettingService _settingService;

        public SettingController(ISettingService settingService)
        {
            this._settingService = settingService;
        }

        /// <summary>
        /// 基数设置
        /// </summary>
        /// <returns></returns>
        [Route("", Name = "settingIndex")]
        public IActionResult SettingIndex()
        {
            return View(_settingService.GetAllCache());
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="pk"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [Route("saveSetting", Name = "saveDataSetting")]
        [HttpGet]
        public IActionResult SaveDataSetting(string pk, string value)
        {
            if (String.IsNullOrEmpty(pk) || String.IsNullOrEmpty(value))
            {
                AjaxData.Message = "请输入数值";
                return Json(AjaxData);
            }
            value = value.Trim();
            pk = pk.Trim();
            var reuslt = _settingService.SaveSetting(pk, value);
            AjaxData.Status = reuslt.Status;
            AjaxData.Message = reuslt.Message;
            return Json(AjaxData);
        }

    }
}