﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Layui.Framework.Controllers.Admin;
using Layui.Entities;
using Layui.Services.SysUser;
using Microsoft.Extensions.Caching.Memory;
using Layui.Framework.Security.Admin;
using Layui.Core.Librs;
using Microsoft.AspNetCore.Http;

namespace Layui.Mvc.Areas.Admin.Controllers
{
    /// <summary>
    /// 后台管理登录控制器
    /// </summary>  
    [Route("admin")]
    public class LoginController : AdminAreaController
    {
        private ISysUserService _sysUserService;
        private IMemoryCache _memoryCache;
        private IAdminAuthService _authenticationService;

        public LoginController(ISysUserService sysUserService,
            IAdminAuthService authenticationService,
            IMemoryCache memoryCache)
        {
            this._memoryCache = memoryCache;
            this._sysUserService = sysUserService;
            this._authenticationService = authenticationService;
        }

        /// <summary>
        /// Session
        /// </summary>
        /// <returns></returns>
        [Route("", Name = "adminLogin")]
        public IActionResult LoginIndex()
        {
            AdminLoginModel loginModel = new AdminLoginModel();
            return View(loginModel);
        }

        [HttpPost]
        [Route("")]
        public IActionResult LoginIndex(AdminLoginModel model)
        {
            if (!ModelState.IsValid)
            {
                AjaxData.Message = "请输入用户账号和密码";
                return Json(AjaxData);
            }
            var result = _sysUserService.ValidateUser(model.Account, model.Password);
            AjaxData.Status = result.Status;
            AjaxData.Message = result.Message;
            if (result.Item1)
            {
                _authenticationService.SignIn(result.Token, result.User.Name);
            }
            return Json(AjaxData);
        }

        /// <summary>
        /// 登录之前获取密码盐
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [Route("getSalt",Name = "getSalt")]
        public IActionResult getSalt(string account)
        {
           var user = _sysUserService.GetByAccount(account);
            return Content(user?.Salt);
        }

    }
}