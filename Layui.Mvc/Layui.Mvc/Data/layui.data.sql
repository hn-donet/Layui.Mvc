/*    ==脚本参数==

    源服务器版本 : SQL Server 2017 (14.0.1000)
    源数据库引擎版本 : Microsoft SQL Server Standard Edition
    源数据库引擎类型 : 独立的 SQL Server

    目标服务器版本 : SQL Server 2017
    目标数据库引擎版本 : Microsoft SQL Server Standard Edition
    目标数据库引擎类型 : 独立的 SQL Server
*/
USE [master]
GO
/****** Object:  Database [Layui]    Script Date: 2018/3/2 17:53:05 ******/
CREATE DATABASE [Layui]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Layui', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Layui.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Layui_log', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Layui_0.ldf' , SIZE = 2880KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Layui] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Layui].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Layui] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Layui] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Layui] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Layui] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Layui] SET ARITHABORT OFF 
GO
ALTER DATABASE [Layui] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Layui] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Layui] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Layui] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Layui] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Layui] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Layui] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Layui] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Layui] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Layui] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Layui] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Layui] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Layui] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Layui] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Layui] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Layui] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [Layui] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Layui] SET RECOVERY FULL 
GO
ALTER DATABASE [Layui] SET  MULTI_USER 
GO
ALTER DATABASE [Layui] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Layui] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Layui] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Layui] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Layui] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Layui] SET QUERY_STORE = OFF
GO
USE [Layui]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Layui]
GO
/****** Object:  Table [dbo].[ActivityLog]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityLog](
	[Id] [uniqueidentifier] NOT NULL,
	[ActivityName] [nvarchar](max) NOT NULL,
	[Method] [nvarchar](50) NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[UserId] [uniqueidentifier] NULL,
	[CreationTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[IsMenu] [bit] NOT NULL,
	[SysResource] [nvarchar](400) NOT NULL,
	[ResouceID] [nvarchar](500) NOT NULL,
	[FatherID] [nvarchar](500) NULL,
	[Controller] [nvarchar](500) NULL,
	[Action] [nvarchar](500) NULL,
	[RouteName] [nvarchar](500) NULL,
	[CssClass] [nvarchar](500) NULL,
	[Sort] [int] NOT NULL,
	[IsDisabled] [bit] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Setting]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setting](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysDomain]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysDomain](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DomainType] [int] NOT NULL,
	[Domain] [nvarchar](500) NOT NULL,
	[CreationTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysLog]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysLog](
	[Id] [uniqueidentifier] NOT NULL,
	[Level] [int] NOT NULL,
	[ShortMessage] [nvarchar](max) NOT NULL,
	[FullMessage] [nvarchar](max) NULL,
	[IpAddress] [nvarchar](50) NULL,
	[PageUrl] [nvarchar](max) NULL,
	[ReferrerUrl] [nvarchar](max) NULL,
	[CreationTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysPermission]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysPermission](
	[Id] [uniqueidentifier] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreationTime] [datetime] NOT NULL,
 CONSTRAINT [PK_PermissionRecord] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysRole]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysRole](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreationTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NULL,
	[ModifiedTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysStore]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysStore](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[SslEnabled] [bit] NOT NULL,
	[Disabled] [bit] NOT NULL,
	[IconClass] [nvarchar](500) NOT NULL,
	[CompanyName] [nvarchar](500) NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreationTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NULL,
	[ModifiedTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysUser]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysUser](
	[Id] [uniqueidentifier] NOT NULL,
	[Account] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](max) NULL,
	[MobilePhone] [nvarchar](50) NULL,
	[Password] [nvarchar](max) NOT NULL,
	[Salt] [nvarchar](max) NOT NULL,
	[Sex] [nvarchar](2) NULL,
	[Enabled] [bit] NOT NULL,
	[IsAdmin] [bit] NOT NULL,
	[CreationTime] [datetime] NOT NULL,
	[LoginFailedNum] [int] NOT NULL,
	[AllowLoginTime] [datetime] NULL,
	[LoginLock] [bit] NOT NULL,
	[LastLoginTime] [datetime] NULL,
	[LastIpAddress] [nvarchar](50) NULL,
	[LastActivityTime] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletedTime] [datetime] NULL,
	[ModifiedTime] [datetime] NULL,
	[Modifier] [uniqueidentifier] NULL,
	[Creator] [uniqueidentifier] NULL,
	[Avatar] [image] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysUserLoginLog]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysUserLoginLog](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[IpAddress] [nvarchar](50) NOT NULL,
	[LoginTime] [datetime] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysUserNote]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysUserNote](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[MsgContent] [nvarchar](500) NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreationTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SysUserNote] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysUserRole]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysUserRole](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysUserToken]    Script Date: 2018/3/2 17:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysUserToken](
	[Id] [uniqueidentifier] NOT NULL,
	[SysUserId] [uniqueidentifier] NOT NULL,
	[ExpireTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Category]    Script Date: 2018/3/2 17:53:05 ******/
ALTER TABLE [dbo].[Category] ADD  CONSTRAINT [IX_Category] UNIQUE NONCLUSTERED 
(
	[SysResource] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActivityLog] ADD  CONSTRAINT [DF_ActivityLog_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[ActivityLog] ADD  CONSTRAINT [DF_ActivityLog_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
GO
ALTER TABLE [dbo].[Category] ADD  CONSTRAINT [DF_Category_Sort]  DEFAULT ((100)) FOR [Sort]
GO
ALTER TABLE [dbo].[Category] ADD  CONSTRAINT [DF_Category_IsDisabled]  DEFAULT ((0)) FOR [IsDisabled]
GO
ALTER TABLE [dbo].[Setting] ADD  CONSTRAINT [DF_Setting_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[SysDomain] ADD  CONSTRAINT [DF_SysDomain_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
GO
ALTER TABLE [dbo].[SysLog] ADD  CONSTRAINT [DF_SysLog_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[SysLog] ADD  CONSTRAINT [DF_SysLog_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
GO
ALTER TABLE [dbo].[SysPermission] ADD  CONSTRAINT [DF_PermissionRecord_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[SysPermission] ADD  CONSTRAINT [DF_PermissionRecord_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
GO
ALTER TABLE [dbo].[SysRole] ADD  CONSTRAINT [DF_SysRole_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[SysRole] ADD  CONSTRAINT [DF_SysRole_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
GO
ALTER TABLE [dbo].[SysStore] ADD  CONSTRAINT [DF_SysStore_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[SysStore] ADD  CONSTRAINT [DF_SysStore_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
GO
ALTER TABLE [dbo].[SysUser] ADD  CONSTRAINT [DF_SysUser_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[SysUser] ADD  CONSTRAINT [DF_SysUser_Enabled]  DEFAULT ((1)) FOR [Enabled]
GO
ALTER TABLE [dbo].[SysUser] ADD  CONSTRAINT [DF_SysUser_IsAdmin]  DEFAULT ((0)) FOR [IsAdmin]
GO
ALTER TABLE [dbo].[SysUser] ADD  CONSTRAINT [DF_SysUser_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
GO
ALTER TABLE [dbo].[SysUser] ADD  CONSTRAINT [DF_SysUser_LoginFailedNum]  DEFAULT ((0)) FOR [LoginFailedNum]
GO
ALTER TABLE [dbo].[SysUser] ADD  CONSTRAINT [DF_SysUser_LoginLock]  DEFAULT ((0)) FOR [LoginLock]
GO
ALTER TABLE [dbo].[SysUser] ADD  CONSTRAINT [DF_SysUser_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SysUserNote] ADD  CONSTRAINT [DF_SysUserNote_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[SysUserNote] ADD  CONSTRAINT [DF_SysUserNote_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
GO
ALTER TABLE [dbo].[SysUserRole] ADD  CONSTRAINT [DF_SysUserRole_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[ActivityLog]  WITH CHECK ADD  CONSTRAINT [FK_ActivityLog_SysUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[ActivityLog] CHECK CONSTRAINT [FK_ActivityLog_SysUser]
GO
ALTER TABLE [dbo].[SysPermission]  WITH CHECK ADD  CONSTRAINT [FK_PermissionRecord_SysRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[SysRole] ([Id])
GO
ALTER TABLE [dbo].[SysPermission] CHECK CONSTRAINT [FK_PermissionRecord_SysRole]
GO
ALTER TABLE [dbo].[SysPermission]  WITH CHECK ADD  CONSTRAINT [FK_PermissionRecord_SysUser] FOREIGN KEY([Creator])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysPermission] CHECK CONSTRAINT [FK_PermissionRecord_SysUser]
GO
ALTER TABLE [dbo].[SysPermission]  WITH CHECK ADD  CONSTRAINT [FK_SysPermission_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([Id])
GO
ALTER TABLE [dbo].[SysPermission] CHECK CONSTRAINT [FK_SysPermission_Category]
GO
ALTER TABLE [dbo].[SysRole]  WITH CHECK ADD  CONSTRAINT [FK_SysRole_SysUser] FOREIGN KEY([Creator])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysRole] CHECK CONSTRAINT [FK_SysRole_SysUser]
GO
ALTER TABLE [dbo].[SysRole]  WITH CHECK ADD  CONSTRAINT [FK_SysRole_SysUser1] FOREIGN KEY([Modifier])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysRole] CHECK CONSTRAINT [FK_SysRole_SysUser1]
GO
ALTER TABLE [dbo].[SysStore]  WITH CHECK ADD  CONSTRAINT [FK_SysStore_SysUser] FOREIGN KEY([Creator])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysStore] CHECK CONSTRAINT [FK_SysStore_SysUser]
GO
ALTER TABLE [dbo].[SysStore]  WITH CHECK ADD  CONSTRAINT [FK_SysStore_SysUser_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysStore] CHECK CONSTRAINT [FK_SysStore_SysUser_Modifier]
GO
ALTER TABLE [dbo].[SysUser]  WITH CHECK ADD  CONSTRAINT [FK_SysUser_SysUser] FOREIGN KEY([Id])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysUser] CHECK CONSTRAINT [FK_SysUser_SysUser]
GO
ALTER TABLE [dbo].[SysUser]  WITH CHECK ADD  CONSTRAINT [FK_SysUser_SysUser_creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysUser] CHECK CONSTRAINT [FK_SysUser_SysUser_creator]
GO
ALTER TABLE [dbo].[SysUserLoginLog]  WITH CHECK ADD  CONSTRAINT [FK_SysUserLoginLog_SysUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysUserLoginLog] CHECK CONSTRAINT [FK_SysUserLoginLog_SysUser]
GO
ALTER TABLE [dbo].[SysUserNote]  WITH CHECK ADD  CONSTRAINT [FK_SysUserNote_SysUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysUserNote] CHECK CONSTRAINT [FK_SysUserNote_SysUser]
GO
ALTER TABLE [dbo].[SysUserNote]  WITH CHECK ADD  CONSTRAINT [FK_SysUserNote_SysUser_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysUserNote] CHECK CONSTRAINT [FK_SysUserNote_SysUser_Creator]
GO
ALTER TABLE [dbo].[SysUserRole]  WITH CHECK ADD  CONSTRAINT [FK_SysUserRole_SysRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[SysRole] ([Id])
GO
ALTER TABLE [dbo].[SysUserRole] CHECK CONSTRAINT [FK_SysUserRole_SysRole]
GO
ALTER TABLE [dbo].[SysUserRole]  WITH CHECK ADD  CONSTRAINT [FK_SysUserRole_SysUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysUserRole] CHECK CONSTRAINT [FK_SysUserRole_SysUser]
GO
ALTER TABLE [dbo].[SysUserToken]  WITH CHECK ADD  CONSTRAINT [FK_SysUserToken_SysUser] FOREIGN KEY([SysUserId])
REFERENCES [dbo].[SysUser] ([Id])
GO
ALTER TABLE [dbo].[SysUserToken] CHECK CONSTRAINT [FK_SysUserToken_SysUser]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'禁用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Category', @level2type=N'COLUMN',@level2name=N'IsDisabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色权限数据列表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SysPermission'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户表操作记录' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SysUserNote'
GO
USE [master]
GO
ALTER DATABASE [Layui] SET  READ_WRITE 
GO
