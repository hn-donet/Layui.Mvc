﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Layui.Framework.Infrastructure
{
    public interface IWorkContext
    {
        /// <summary>
        /// 当前登录用户
        /// </summary>
        Entities.SysUser CurrentUser { get; }

        /// <summary>
        /// 当前登录用户的菜单
        /// </summary>
        List<Entities.Category> Categories { get; }

        /// <summary>
        /// 验证权限
        /// </summary>
        /// <param name="routeName"></param>
        /// <returns></returns>
        bool OwnPermission(string routeName);
    }
}
