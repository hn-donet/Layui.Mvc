﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Layui.Core;
using Layui.Services.SysLog;

namespace Layui.Framework.Filters
{
    public class WebExceptionFilter : ExceptionFilterAttribute
    {
        private ISysLogService _sysLogService;

        public WebExceptionFilter(ISysLogService sysLogService)
        {
            this._sysLogService = sysLogService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnException(ExceptionContext context)
        {
            _sysLogService.Error(context.Exception.Message, context.Exception);
            context.Result = new ViewResult() { ViewName = "Error" };
        }
    }
}
