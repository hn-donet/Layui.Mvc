﻿using Layui.Framework.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using Layui.Entities;
using Layui.Framework.Security.Admin;
using Layui.Framework.Menu;
using System.Linq;

namespace Layui.Framework
{
    public class WorkContext : IWorkContext
    {
        private IAdminAuthService _adminAuthService;

        public WorkContext(IAdminAuthService authenticationService)
        {
            this._adminAuthService = authenticationService;
        }

        /// <summary>
        /// 当前登录用户
        /// </summary>
        public SysUser CurrentUser
        {
            get { return _adminAuthService.GetCurrentUser(); }
        }

        /// <summary>
        /// 当前登录用户菜单
        /// </summary>
        public List<Category> Categories {
            get
            {
              return _adminAuthService.GetMyCategories();
            }
        }

        /// <summary>
        /// 验证权限
        /// </summary>
        /// <param name="routeName"></param>
        /// <returns></returns>
        public bool OwnPermission(string routeName)
        {
            return _adminAuthService.AuthorizePermission(routeName);
        }

    }
}
